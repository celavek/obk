use serde::{de, Deserialize, Deserializer};

use std::convert::From;

use super::{BINANCE_ID, BITSTAMP_ID};

pub const BINANCE_WSS_URI: &str = "wss://stream.binance.com:9443/ws/";
pub const BINANCE_WSS_OPTIONS: &str = "@depth20@100ms";
pub const BITSTAMP_WSS_URI: &str = "wss://ws.bitstamp.net";
pub const BITSTAMP_WSS_OPTIONS: &str = r#"{ "event": "bts:subscribe", "data": { "channel": "#;

///
/// Bid or ask entry in the data stream.
///
#[derive(Clone, Debug, Deserialize, Default)]
pub struct RawBookEntry {
    #[serde(deserialize_with = "de_float_from_str")]
    pub price: f64,
    #[serde(deserialize_with = "de_float_from_str")]
    pub amount: f64,
}

///
/// Binance specific stream output format for the websocket API.
///
#[derive(Clone, Debug, Deserialize, Default)]
pub struct BinanceStream {
    #[serde(rename = "lastUpdateId")]
    pub last_update_id: usize,
    pub bids: Vec<RawBookEntry>,
    pub asks: Vec<RawBookEntry>,
}

///
/// Bitstamp specific data entry in the websocket stream output.
///
#[derive(Clone, Debug, Deserialize, Default)]
pub struct BitstampEntry {
    #[serde(deserialize_with = "de_long_from_str")]
    pub timestamp: u64,
    #[serde(deserialize_with = "de_long_from_str")]
    pub microtimestamp: u64,
    pub bids: Vec<RawBookEntry>,
    pub asks: Vec<RawBookEntry>,
}

///
/// Bitstamp specific stream output format for the websocket API.
///
#[derive(Clone, Debug, Deserialize, Default)]
pub struct BitstampStream {
    pub data: BitstampEntry,
    pub channel: String,
    pub event: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum OrderbookStream {
    BinanceStream(BinanceStream),
    BitstampStream(BitstampStream),
}

/// Conversion from OrderbookStream to String.
impl From<OrderbookStream> for String {
    fn from(item: OrderbookStream) -> Self {
        match item {
            OrderbookStream::BinanceStream(_) => BINANCE_ID.to_owned(),
            OrderbookStream::BitstampStream(_) => BITSTAMP_ID.to_owned(),
        }
    }
}

///
/// Deserialize a floating point number represented as a string, from
/// the server's JSON response.
///
pub fn de_float_from_str<'a, D>(deserializer: D) -> Result<f64, D::Error>
where
    D: Deserializer<'a>,
{
    let str_val = String::deserialize(deserializer)?;
    str_val.parse::<f64>().map_err(de::Error::custom)
}

///
/// Deserialize a large integer number represented as a string, from
/// the server's JSON response.
///
pub fn de_long_from_str<'a, D>(deserializer: D) -> Result<u64, D::Error>
where
    D: Deserializer<'a>,
{
    let str_val = String::deserialize(deserializer)?;
    str_val.parse::<u64>().map_err(de::Error::custom)
}

#[cfg(test)]
#[path = "orderbook_tests.rs"]
mod orderbook_tests;
