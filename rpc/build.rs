/// Build script used to generate the bindings for the protocol buffers
///

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let proto_file = "protos/orderbook_aggregator.proto";

    tonic_build::configure()
        .build_server(true)
        .out_dir("./src/")
        .compile(&[proto_file], &["."])
        .unwrap_or_else(|e| panic!("Protocol buffer compilation error: {}", e));

    println!("cargo:rerun-if-changed={}", proto_file);

    Ok(())
}
