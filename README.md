# Rust learning project

## The problem statement

Using Rust as the programming language create an application that:

1. Connects to two or motr crypto exchanges' websocket feeds at the same time.
2. Pulls order books, using these streaming connections, for a given traded pair of currencies that is configurable 
on the command line, from each exchange.
3. Merges and sorts the order books to create a combined order book.
4. Publishes the spread, top ten bids, and top ten asks from the combined book, as a stream, through a gRPC server.

## Background

### Markets
A market is generally a pair of currencies and an exchange where they are traded. For example, ETH (Ethereum) 
and BTC(Bitcoin) are a pair that together from a traded ‘symbol’ - ETHBTC. This means you can buy or sell ETH 
using BTC as the **pricing** currency.

### Order books
Orders at which people are prepared to buy and sell are send to an exchange, such as Binance. The exchange will 
usually match the buy and sell orders that approach a market ‘mid-price’. The **difference** between the **best ask price** 
and the **best bid price** is called **the spread**. The final, merged order book should have the best deals first. 
That means, if I am selling currency and I want to be the first one to sell, I should be at the best position for 
that, which means I am selling the largest amount at the lowest price. Think about that when sorting each side of the
order book.

### gRPC
GRPC ( https://grpc.io/ ) is relatively modern Remote Procedure Call protocol. If you have used anything like GraphQL 
or Thrift, it should be fairly familiar. If not, it is not difficult to learn! If you are new to RPC you may even find 
you prefer gRPC’s structured and typed protocol over HTTPMethods.
For this challenge you can use this protobuf schema to create your gRPC server :

```
syntax = "proto3";
package orderbook;

service OrderbookAggregator {
	rpc BookSummary(Empty) returns (stream Summary);
}
message Empty {}

message Summary {
	double spread = 1;
	repeated Level bids = 2;
	repeated Level asks = 3;
}

message Level {
	string exchange = 1;
	double price = 2;
	double amount = 3;
}
```

### Relevance
Calculating and streaming prices for markets as part of liquidity analysis is an important aspect of crypto markets and 
other financial markets in general. By gathering order books we can use the top bids and asks to find a fair mid-price. 
Once we have a mid-price for a market, we can calculate strategies for opening orders in that market. That journey begins 
with first streaming the current public market order-books that could be picked up by data aggregation and analytics
services.

## Further information
Two of the most know exchanges that could be used to jumpstart this project, are Binance and Bitstamp.

### Binance
Docs for Websocket connection: https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md
Example API feed: https://api.binance.com/api/v3/depth?symbol=ETHBTC
Websocket connection URL for Binance: wss://stream.binance.com:9443/ws/ethbtc@depth20@100ms

### Bitstamp
Docs: https://www.bitstamp.net/websocket/v2/
Example API feed: https://www.bitstamp.net/api/v2/order_book/ethbtc/
Example Websocket usage: https://www.bitstamp.net/s/webapp/examples/order_book_v2.html

## Requirements
For the configured symbol (e.g. ETH/BTC) the code should fetch the 10 best/lowest asks and the best/highest bids. 
The output will be a stream of an orderbook created from merging the exchanges' books. The output should be standardised 
to a type of format (with 10 asks and 10 bids) similar to:

```
{
	“spread”: 2.72,
	“asks”: [
		{ exchange: "binance", price: 8491.25, amount: 0.008 },
		{ exchange: "coinbase", price: 8496.37, amount: 0.0303 },
		...
	],
	“bids”: [
		{ exchange: "binance", price: 8488.53, amount: 0.002 },
		{ exchange: "kraken", price: 8484.71, amount: 1.0959 },
		...
	]
}
```

Provide a pair (that will exist in the exchanges) and have the server stream the spread and the top 10 asks and bids 
for each exchange, on every change of any order book.
Speed is one of the most important factor for such a service operationally.
