use clap::Parser;
use tungstenite::{connect, Message};
use url::Url;

extern crate obk;
use obk::model::orderbook::{BinanceStream, BitstampStream};

#[derive(Parser, Debug)]
#[clap(
    version = "0.1",
    author = "Marius Cetateanu",
    about = "Order books fetcher"
)]
struct Opts {
    #[clap(short, long, default_value = "ETHBTC")]
    pair: String,
}

static BINANCE_WSS_URI: &str = "wss://stream.binance.com:9443/ws/";
static BINANCE_WSS_OPTIONS: &str = "@depth20@100ms";
static BITSTAMP_WSS_URI: &str = "wss://ws.bitstamp.net";
static BITSTAMP_WSS_OPTIONS: &str = r#"{ "event": "bts:subscribe", "data": { "channel": "#;

fn main() {
    let opts = Opts::parse();
    let pair: String = opts
        .pair
        .parse()
        .expect("Couldn't parse the crypto symbols pair");
    println!("Fetching order book data for pair: {}", pair);

    // Binance
    let binance_url = format!(
        "{}{}{}",
        BINANCE_WSS_URI,
        pair.to_lowercase(),
        BINANCE_WSS_OPTIONS
    );
    let (mut socket, response) =
        connect(Url::parse(&binance_url).unwrap()).expect("Could not connect to Binance");

    println!("Connected to Binance stream");
    println!("HTTP status code: {}", response.status());
    println!("Response headers: ");
    for (ref header, header_value) in response.headers() {
        println!(" - {}: {:?}", header, header_value);
    }

    let msg = socket
        .read_message()
        .expect("Error reading from the socket");
    println!("Got message: {}", msg);
    let msg = match msg {
        Message::Text(s) => s,
        _ => {
            panic!("Error getting text of stream data");
        }
    };

    let parsed = serde_json::from_str::<BinanceStream>(&msg).expect("Cannot parse data");
    for i in 0..parsed.asks.len() {
        println!(
            "{}. ask: {}, amount: {}",
            i, parsed.asks[i].price, parsed.asks[i].amount
        );
    }
    println!("------------------------------------------");
    for i in 0..parsed.bids.len() {
        println!(
            "{}. bid: {}, amount: {}",
            i, parsed.bids[i].price, parsed.bids[i].amount
        );
    }

    println!("\n########################################\n");

    // Bitstamp
    let bitstamp_url = BITSTAMP_WSS_URI.to_string();
    let (mut socket_bstamp, response) =
        connect(Url::parse(&bitstamp_url).unwrap()).expect("Could not connect to Bitstamp");

    println!("Connected to Bitstamp stream");
    println!("HTTP status code: {}", response.status());
    println!("Response headers: ");
    for (ref header, header_value) in response.headers() {
        println!(" - {}: {:?}", header, header_value);
    }

    let pair_option = format!("{}{}{}", "\"order_book_", pair.to_lowercase(), "\"} }");
    println!("{}{}", BITSTAMP_WSS_OPTIONS, pair_option);
    socket_bstamp
        .write_message(Message::Text(format!(
            "{}{}",
            BITSTAMP_WSS_OPTIONS, pair_option
        )))
        .unwrap();

    let msg = socket_bstamp
        .read_message()
        .expect("Error reading response from server");
    println!("Received: {}", msg);
    let msg = socket_bstamp
        .read_message()
        .expect("Error reading response from server");
    /*let msg = match msg {
        Message::Text(s) => s,
        _ => {
            panic!("Error getting text of stream data");
        }
    };*/
    println!("Received from BITSTAMP: {}", msg);

    let parsed =
        serde_json::from_str::<BitstampStream>(&msg.to_string()).expect("Cannot parse data");
    for i in 0..parsed.data.asks.len() {
        println!(
            "{}. ask: {}, amount: {}",
            i, parsed.data.asks[i].price, parsed.data.asks[i].amount
        );
    }
    println!("------------------------------------------");
    for i in 0..parsed.data.bids.len() {
        println!(
            "{}. bid: {}, amount: {}",
            i, parsed.data.bids[i].price, parsed.data.bids[i].amount
        );
    }

    println!("We are going to receive in a loop ...");

    /*loop {
        // this panics at some point I guess due to the fact that it iterates too fast for the server to respond??
        // although that is to be questioned as the stream is set to update every 100ms
        /*let msg = socket.read_message().expect("Error reding from the socket");
        let msg = match msg {
            Message::Text(s) => s,
            _ => {
                panic!("Error getting text of stream data");
                //println!("Error getting text of stream data from Binance!");
            }
        };

        let parsed: OrderStream = serde_json::from_str(&msg).expect("Cannot parse data");
        println!("Received from Binance: ");
        for i in 0..parsed.asks.len() {
            println!("{}. ask: {}, size: {}", i, parsed.asks[i].price, parsed.asks[i].size);
        }
        println!("------------------------------------------");
        for i in 0..parsed.bids.len() {
            println!("{}. bid: {}, size: {}", i, parsed.bids[i].price, parsed.bids[i].size);
        }*/

        println!("\n########################################\n");

        //let msg = socket_bstamp.read_message().expect("Error reading response from server");
        //println!("Received: {}", msg);
        let msg = socket_bstamp.read_message().expect("Error reading response from server");
        /*let msg = match msg {
            Message::Text(s) => s,
            _ => {
                panic!("Error getting text of stream data");
            }
        };*/
        println!("Received from BITSTAMP: {}", msg);
    }*/
}
