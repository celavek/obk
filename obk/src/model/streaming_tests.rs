#[cfg(test)]
mod exchange_tests {

    use super::super::super::orderbook::{
        BinanceStream, BitstampStream, OrderbookStream, RawBookEntry, BINANCE_WSS_OPTIONS,
        BINANCE_WSS_URI, BITSTAMP_WSS_URI,
    };
    use super::super::*;
    //use crate::model::streaming::StreamHandle;

    /*static BINANCE_WSS_URI: &str = "wss://stream.binance.com:9443/ws/";
    static BINANCE_WSS_OPTIONS: &str = "@depth20@100ms";
    static BITSTAMP_WSS_URI: &str = "wss://ws.bitstamp.net";
    static BITSTAMP_WSS_OPTIONS: &str = r#"{ "event": "bts:subscribe", "data": { "channel": "#;*/

    #[tokio::test]
    async fn test_binance_connection() {
        let pair: String = "ETHBTC".to_string();
        let binance_url = format!(
            "{}{}{}",
            BINANCE_WSS_URI,
            pair.to_lowercase(),
            BINANCE_WSS_OPTIONS
        );
        let _binance_stream = StreamHandle::new(
            &binance_url,
            &pair,
            OrderbookStream::BinanceStream(BinanceStream {
                last_update_id: 0,
                bids: vec![],
                asks: vec![],
            }),
        )
        .await;
    }

    #[tokio::test]
    async fn test_bitstamp_connection() {
        let pair: String = "ETHBTC".to_string();
        let bitstamp_url = format!("{}", BITSTAMP_WSS_URI);
        let _bitstamp_stream = StreamHandle::new(
            &bitstamp_url,
            &pair,
            OrderbookStream::BitstampStream(BitstampStream {
                ..Default::default()
            }),
        )
        .await;
    }

    #[test]
    fn test_generate_summary() {
        let raw_asks = vec![
            RawBookEntry {
                price: 8500.25,
                amount: 0.008,
            },
            RawBookEntry {
                price: 8496.37,
                amount: 0.0303,
            },
            RawBookEntry {
                price: 8498.25,
                amount: 0.008,
            },
            RawBookEntry {
                price: 8497.67,
                amount: 0.0303,
            },
            RawBookEntry {
                price: 8498.15,
                amount: 0.008,
            },
            RawBookEntry {
                price: 8498.37,
                amount: 0.0303,
            },
            RawBookEntry {
                price: 8498.45,
                amount: 0.008,
            },
            RawBookEntry {
                price: 8498.67,
                amount: 0.0303,
            },
            RawBookEntry {
                price: 8499.25,
                amount: 0.008,
            },
            RawBookEntry {
                price: 8499.37,
                amount: 0.0303,
            },
        ];
        let raw_bids = vec![
            RawBookEntry {
                price: 8481.53,
                amount: 0.002,
            },
            RawBookEntry {
                price: 8486.71,
                amount: 1.0959,
            },
            RawBookEntry {
                price: 8483.53,
                amount: 0.002,
            },
            RawBookEntry {
                price: 8485.31,
                amount: 1.0959,
            },
            RawBookEntry {
                price: 8486.13,
                amount: 0.002,
            },
            RawBookEntry {
                price: 8485.99,
                amount: 1.0959,
            },
            RawBookEntry {
                price: 8485.89,
                amount: 0.002,
            },
            RawBookEntry {
                price: 8485.71,
                amount: 1.0959,
            },
            RawBookEntry {
                price: 8485.53,
                amount: 0.002,
            },
            RawBookEntry {
                price: 8484.71,
                amount: 1.0959,
            },
        ];

        let summary = Stream::generate_summary(&raw_bids[..4], &raw_asks[..4], BITSTAMP_ID);

        let bids = vec![
            AggBookEntry::Bid(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8486.71,
                    amount: 1.0959,
                },
            }),
            AggBookEntry::Bid(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8485.31,
                    amount: 1.0959,
                },
            }),
            AggBookEntry::Bid(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8483.53,
                    amount: 0.002,
                },
            }),
            AggBookEntry::Bid(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8481.53,
                    amount: 0.002,
                },
            }),
        ];

        let asks = vec![
            AggBookEntry::Ask(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8496.37,
                    amount: 0.0303,
                },
            }),
            AggBookEntry::Ask(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8497.67,
                    amount: 0.0303,
                },
            }),
            AggBookEntry::Ask(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8498.25,
                    amount: 0.008,
                },
            }),
            AggBookEntry::Ask(BookEntry {
                exchange: BITSTAMP_ID.to_owned(),
                book: BookPair {
                    price: 8500.25,
                    amount: 0.008,
                },
            }),
        ];

        let expected = (bids, asks);
        assert_eq!(summary, expected, "Testing aggregation summary generation");
    }
}
