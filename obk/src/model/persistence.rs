use async_trait::async_trait;

use super::aggregate::AggregatorHandle;
use obk_rpc::{orderbook_aggregator::Summary, server::ObkPersistence};

#[derive(Debug)]
pub struct AggregatePersistence {
    handle: AggregatorHandle,
}

impl AggregatePersistence {
    pub fn new(handle: AggregatorHandle) -> Self {
        AggregatePersistence { handle }
    }
}

#[async_trait]
impl ObkPersistence for AggregatePersistence {
    async fn fetch_summary(&self) -> Result<Summary, Box<dyn std::error::Error>> {
        Ok(self.handle.read().await)
    }
}
