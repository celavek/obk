use async_trait::async_trait;

use crate::orderbook_aggregator::{Level, Summary};
use crate::server::ObkPersistence;

#[derive(Debug)]
pub struct MockPersistence {}

#[allow(clippy::redundant_clone)]
#[async_trait]
impl ObkPersistence for MockPersistence {
    async fn fetch_summary(&self) -> Result<Summary, Box<dyn std::error::Error>> {
        let binance = "binance".to_string();
        let bitstamp = "bitstamp".to_string();
        let kraken = "kraken".to_string();
        let coinbase = "coinbase".to_string();

        Ok(Summary {
            spread: 2.72,
            asks: vec![
                Level {
                    exchange: binance.to_owned(),
                    price: 8491.25,
                    amount: 0.008,
                },
                Level {
                    exchange: coinbase.to_owned(),
                    price: 8496.37,
                    amount: 0.0303,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8497.25,
                    amount: 0.008,
                },
                Level {
                    exchange: coinbase.to_owned(),
                    price: 8497.67,
                    amount: 0.0303,
                },
                Level {
                    exchange: kraken.to_owned(),
                    price: 8498.15,
                    amount: 0.008,
                },
                Level {
                    exchange: kraken.to_owned(),
                    price: 8498.37,
                    amount: 0.0303,
                },
                Level {
                    exchange: binance.to_owned(),
                    price: 8498.45,
                    amount: 0.008,
                },
                Level {
                    exchange: coinbase.to_owned(),
                    price: 8498.67,
                    amount: 0.0303,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8499.25,
                    amount: 0.008,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8499.37,
                    amount: 0.0303,
                },
            ],
            bids: vec![
                Level {
                    exchange: binance.to_owned(),
                    price: 8488.53,
                    amount: 0.002,
                },
                Level {
                    exchange: kraken.to_owned(),
                    price: 8486.71,
                    amount: 1.0959,
                },
                Level {
                    exchange: binance.to_owned(),
                    price: 8486.53,
                    amount: 0.002,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8486.31,
                    amount: 1.0959,
                },
                Level {
                    exchange: binance.to_owned(),
                    price: 8486.13,
                    amount: 0.002,
                },
                Level {
                    exchange: kraken.to_owned(),
                    price: 8485.99,
                    amount: 1.0959,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8485.89,
                    amount: 0.002,
                },
                Level {
                    exchange: binance.to_owned(),
                    price: 8485.71,
                    amount: 1.0959,
                },
                Level {
                    exchange: bitstamp.to_owned(),
                    price: 8485.53,
                    amount: 0.002,
                },
                Level {
                    exchange: kraken.to_owned(),
                    price: 8484.71,
                    amount: 1.0959,
                },
            ],
        })
    }
}
