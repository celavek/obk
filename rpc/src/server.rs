use async_trait::async_trait;
use core::fmt::Debug;
use futures::Stream;
use std::pin::Pin;
use tokio::sync::mpsc;
use tokio_stream::{wrappers::ReceiverStream, StreamExt};
use tonic::{Request, Response, Status};
use tracing::{debug, instrument, trace};

use crate::orderbook_aggregator::orderbook_aggregator_server::OrderbookAggregator;
use crate::orderbook_aggregator::{Empty, Summary};

#[async_trait]
pub trait ObkPersistence: Debug + Send + Sync + 'static {
    async fn fetch_summary(&self) -> Result<Summary, Box<dyn std::error::Error>>;
}

#[derive(Debug)]
pub struct OrderbookAggregatorImpl {
    pub persistence: Box<dyn ObkPersistence>,
}

#[tonic::async_trait]
impl OrderbookAggregator for OrderbookAggregatorImpl {
    type BookSummaryStream =
        Pin<Box<dyn Stream<Item = Result<Summary, Status>> + Send + Sync + 'static>>;

    #[instrument]
    async fn book_summary(
        &self,
        request: Request<Empty>,
    ) -> Result<tonic::Response<Self::BookSummaryStream>, tonic::Status> {
        debug!("Request from {:?}", request.remote_addr());

        let response = self
            .persistence
            .fetch_summary()
            .await
            .expect("Cannot retrive summary for orderbook");
        trace!(
            "{:?}{:?}{:?}",
            response.spread,
            response.bids,
            response.asks
        );

        let mut outgoing = Box::pin(tokio_stream::iter(vec![response]));
        let (tx, rx) = mpsc::channel(32);
        tokio::spawn(async move {
            while let Some(item) = outgoing.next().await {
                match tx.send(Result::<_, Status>::Ok(item)).await {
                    Ok(_) => {
                        debug!("Message queued successfully")
                    }
                    Err(item) => {
                        debug!("Outupt stream will be dropped because {:?}", item);
                        break;
                    }
                }
            }
        });

        debug!("Status OK. Sending response to {:?}", request.remote_addr());
        Ok(Response::new(Box::pin(ReceiverStream::new(rx))))
    }
}
