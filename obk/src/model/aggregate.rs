use chrono::Utc;
use float_eq::{derive_float_eq, float_eq};
use itertools::kmerge;
use std::collections::HashMap;
use std::convert::From;
use std::sync::{Arc, Mutex};
use std::{cmp::Ordering, default::Default};
use tokio::sync::{mpsc, oneshot};

use obk_rpc::orderbook_aggregator::{Level, Summary};

///
/// Structure representing an order entry specific to an aggregation
/// across multiple sources(e.g. exchanges).
///
#[derive_float_eq(
    ulps_tol = "BookPairUlps",
    ulps_tol_derive = "Clone, Copy, Debug, PartialEq",
    debug_ulps_diff = "BookPairDebugUlpsDiff",
    debug_ulps_diff_derive = "Clone, Copy, Debug, PartialEq"
)]
#[derive(Debug, Clone, Copy)]
pub struct BookPair {
    pub price: f64,
    pub amount: f64,
}

impl Eq for BookPair {}

impl PartialEq for BookPair {
    fn eq(&self, other: &Self) -> bool {
        float_eq!(self.price, other.price, abs <= 0.00000000_1)
            && float_eq!(self.amount, other.amount, abs <= 0.00000000_1)
    }
}

///
/// Structure representing an orderbook entry with information about the exchange
/// and a book pair.
///
#[derive(Debug, Clone, Eq)]
pub struct BookEntry {
    pub exchange: String,
    pub book: BookPair,
}

impl PartialEq for BookEntry {
    fn eq(&self, other: &Self) -> bool {
        float_eq!(
            self.book,
            other.book,
            abs <= BookPair {
                price: 0.00000000_1,
                amount: 0.00000000_1
            }
        )
    }
}
///
/// Enumeration with variants for the bids and asks entries of an orderbook.
/// Although the bids and asks have the same fields, the order relation is
/// differently implemented for the variants.
///
/// The enum implements the Eq and PartialEq traits as it will be used with ordered
/// collections.
///
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum AggBookEntry {
    Bid(BookEntry),
    Ask(BookEntry),
}

///
///  Implementation of the <code>Ord</code> trait for the <code>AggBookEntry</code>.
///
/// The ordering is done taking into account that we want *the item with the highest
/// priority, that is the one to be considered first in case of a buy or sell, to be
/// at the index 0* in the ordered collection! So in terms of the Ord trait implementation
/// the "highest priority" translates as <code>Ordering::Less</code>.
///
/// Order relation for the **bids**:
/// - largest price largest amount compares as the "highest priority".
/// - in case of same price, the largest amount compares as the "highest priority".
///
/// Order relation for the **asks**:
/// - lowest price largest amount compares as the "highest priority".
/// - in case of same price, the largest amount compares as the as the "highest priority".
///
/// Comparing a bid and an ask will always result in the bid as being the "highest
/// priority". I am aware that this particular choice might not be what is correct actually,
/// but it reflects my current understanding of the problem's domain.
///  
#[allow(clippy::needless_return)]
impl Ord for AggBookEntry {
    fn cmp(&self, other: &AggBookEntry) -> Ordering {
        match self {
            AggBookEntry::Bid(lhs) => match other {
                AggBookEntry::Bid(rhs) => {
                    if float_eq!(
                        lhs.book,
                        rhs.book,
                        abs <= BookPair {
                            price: 1.00000000_1,
                            amount: 0.00000000_1
                        }
                    ) {
                        Ordering::Equal
                    } else {
                        if float_eq!(lhs.book.price, rhs.book.price, abs <= 0.00000000_1) {
                            return if lhs.book.amount < rhs.book.amount {
                                Ordering::Greater
                            } else {
                                Ordering::Less
                            };
                        }

                        if lhs.book.price < rhs.book.price {
                            Ordering::Greater
                        } else {
                            Ordering::Less
                        }
                    }
                }
                AggBookEntry::Ask(_rhs) => Ordering::Greater,
            },
            AggBookEntry::Ask(lhs) => match other {
                AggBookEntry::Ask(rhs) => {
                    if float_eq!(
                        lhs.book,
                        rhs.book,
                        abs <= BookPair {
                            price: 1.00000000_1,
                            amount: 0.00000000_1
                        }
                    ) {
                        Ordering::Equal
                    } else {
                        if float_eq!(lhs.book.price, rhs.book.price, abs <= 0.00000000_1) {
                            return if lhs.book.amount < rhs.book.amount {
                                Ordering::Greater
                            } else {
                                Ordering::Less
                            };
                        }

                        return if lhs.book.price < rhs.book.price {
                            Ordering::Less
                        } else {
                            Ordering::Greater
                        };
                    }
                }
                AggBookEntry::Bid(_rhs) => Ordering::Less,
            },
        }
    }
}

///
/// Implementation of the <ccode>PartialOrd</code> trait for <code>AggBookEntry</code>
///
impl PartialOrd for AggBookEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

///
/// Structure representing a collection of order entries specific to an
/// aggregation across multiple sources(e.g. exchanges).
///
#[derive(Debug, Clone)]
pub struct AggSummary {
    pub timestamp: i64,
    pub spread: f64,
    pub bids: Vec<AggBookEntry>,
    pub asks: Vec<AggBookEntry>,
}

impl AggSummary {
    pub fn new(
        timestamp: i64,
        spread: f64,
        bids: Vec<AggBookEntry>,
        asks: Vec<AggBookEntry>,
    ) -> Self {
        AggSummary {
            timestamp,
            spread,
            bids,
            asks,
        }
    }
}

/// Default capacity to provision for the aggregation summary internal buffers.
const DEFAULT_CAPACITY: usize = 20;

///
/// Implementation of the default trait for the aggregation summary struct.
/// Timestamp is set to thecurrent timestamp, spread to 0, and the buffers
/// are initialized with empty vectors.
///
impl Default for AggSummary {
    fn default() -> Self {
        Self {
            timestamp: Utc::now().timestamp(),
            spread: 0.000000,
            bids: Vec::<AggBookEntry>::with_capacity(DEFAULT_CAPACITY),
            asks: Vec::<AggBookEntry>::with_capacity(DEFAULT_CAPACITY),
        }
    }
}

///
/// Message types accepted by an aggregator object.
/// This type of messages will be used to communicate with an
/// aggregator object via the aggregator handle.
///
enum AggMessage {
    Update {
        respond_to: oneshot::Sender<bool>,
        payload: (AggSummary, String),
    },
    Get {
        respond_to: oneshot::Sender<AggSummary>,
    },
}

///
/// The Aggregator is used to aggregate the orderbok streams.
///
struct Aggregator {
    receiver: mpsc::Receiver<AggMessage>,
    bids_streams: Arc<Mutex<HashMap<String, Vec<AggBookEntry>>>>,
    asks_streams: Arc<Mutex<HashMap<String, Vec<AggBookEntry>>>>,
}

const K_HIGHEST_PRIO: usize = 10;

impl Aggregator {
    fn new(receiver: mpsc::Receiver<AggMessage>) -> Self {
        Aggregator {
            receiver,
            bids_streams: Arc::new(Mutex::new(HashMap::new())),
            asks_streams: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    fn handle_message(&mut self, msg: AggMessage) {
        match msg {
            AggMessage::Update {
                respond_to,
                mut payload,
            } => {
                let exchange = payload.1;
                // we only care about the latest data that comes from a certain exchange
                // we always swap out the buffers and leave the aggregation for later on
                // a need to have basis
                {
                    let mut bids = self.bids_streams.lock().unwrap();
                    if let Some(val) = bids.get_mut(&exchange) {
                        std::mem::swap(val, &mut payload.0.bids);
                    } else {
                        bids.insert(exchange.to_owned(), payload.0.bids);
                    }
                }
                {
                    let mut asks = self.asks_streams.lock().unwrap();
                    if let Some(val) = asks.get_mut(&exchange) {
                        std::mem::swap(val, &mut payload.0.asks);
                    } else {
                        asks.insert(exchange.to_owned(), payload.0.asks);
                    }
                }

                let _ = respond_to.send(true);
            }

            AggMessage::Get { respond_to } => {
                let bids: Vec<AggBookEntry> = {
                    let blk = self.bids_streams.lock().unwrap();
                    let all_bids: Vec<Vec<AggBookEntry>> = blk.values().cloned().collect();
                    let bids_agg: Vec<AggBookEntry> = kmerge(all_bids).collect();

                    bids_agg.into_iter().take(K_HIGHEST_PRIO).collect()
                };

                let asks: Vec<AggBookEntry> = {
                    let alk = self.asks_streams.lock().unwrap();
                    let all_asks: Vec<Vec<AggBookEntry>> = alk.values().cloned().collect();
                    let asks_agg: Vec<AggBookEntry> = kmerge(all_asks).collect();

                    asks_agg.into_iter().take(K_HIGHEST_PRIO).collect()
                };

                let mut highest_bid: f64 = 0.0;
                if let AggBookEntry::Bid(entry) = &bids[0] {
                    highest_bid = entry.book.price;
                }

                let mut highest_ask: f64 = 0.0;
                if let AggBookEntry::Ask(entry) = &asks[0] {
                    highest_ask = entry.book.price;
                }

                let sum = AggSummary {
                    timestamp: Utc::now().timestamp(),
                    spread: highest_ask - highest_bid,
                    bids,
                    asks,
                };

                //let (mut buf_input, mut buf_output) = self.get_buffer();
                /*if let Some(buf_input) = &mut self.input {
                    buf_input.write(sum);
                }*/

                let _ = respond_to.send(sum);
            }
        }
    }
}

///
/// Update the aggregator object based on the current message.
/// Message is sent via the aggregator's mpsc's channel receiver and
/// is handled internally.
///
async fn update(mut agg: Aggregator) {
    while let Some(msg) = agg.receiver.recv().await {
        agg.handle_message(msg);
    }
}

///
///
///
#[derive(Debug, Clone)]
pub struct AggregatorHandle {
    sender: mpsc::Sender<AggMessage>,
}

impl AggregatorHandle {
    pub fn new() -> Self {
        let (sender, receiver) = mpsc::channel(16);
        let agg = Aggregator::new(receiver);
        //let output = agg.get_buffer();

        tokio::spawn(update(agg));

        Self {
            sender,
            //output,
        }
    }

    pub async fn write(&self, stream_data: (AggSummary, String)) -> bool {
        let (tx, rx) = oneshot::channel();
        let msg = AggMessage::Update {
            respond_to: tx,
            payload: stream_data,
        };

        let _ = self.sender.send(msg).await;
        rx.await
            .expect("Could not update data - aggregator task has been killed")
    }

    pub async fn read(&self) -> Summary {
        let (tx, rx) = oneshot::channel();
        let msg = AggMessage::Get { respond_to: tx };
        let _ = self.sender.send(msg).await;
        let buffer = rx
            .await
            .expect("Could not aggregate data -  aggregator task has been killed");

        //let buffer = self.output.read();

        Summary {
            spread: buffer.spread,
            bids: buffer.bids.iter().map(Level::from).collect::<Vec<_>>(),
            asks: buffer.asks.iter().map(Level::from).collect::<Vec<_>>(),
        }
    }
}

impl Default for AggregatorHandle {
    fn default() -> Self {
        Self::new()
    }
}

impl From<BookEntry> for Level {
    fn from(val: BookEntry) -> Self {
        Level {
            exchange: val.exchange,
            price: val.book.price,
            amount: val.book.amount,
        }
    }
}

impl From<&AggBookEntry> for Level {
    fn from(val: &AggBookEntry) -> Self {
        match val {
            AggBookEntry::Bid(bid) => Level {
                exchange: bid.exchange.to_owned(),
                price: bid.book.price,
                amount: bid.book.amount,
            },
            AggBookEntry::Ask(ask) => Level {
                exchange: ask.exchange.to_owned(),
                price: ask.book.price,
                amount: ask.book.amount,
            },
        }
    }
}

#[cfg(test)]
#[path = "aggregate_tests.rs"]
mod aggregate_tests;
