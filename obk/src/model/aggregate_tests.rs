#[cfg(test)]
mod aggregate_tests {
    use super::super::*;

    #[test]
    fn test_cmp_for_agg_bid_high_prio() {
        let lhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.15,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Less);
    }

    #[test]
    fn test_cmp_for_agg_bid_low_prio() {
        let lhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.83,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Greater);
    }

    #[test]
    fn test_cmp_for_agg_bid_high_amount() {
        let lhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Less);
    }

    #[test]
    fn test_cmp_for_agg_bid_eq() {
        let lhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Equal);
    }

    #[test]
    fn test_cmp_for_agg_ask_high_prio() {
        let lhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28039.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.15,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Less);
    }

    #[test]
    fn test_cmp_for_agg_ask_low_prio() {
        let lhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.15,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Greater);
    }

    #[test]
    fn test_cmp_for_agg_ask_high_amount() {
        let lhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Less);
    }

    #[test]
    fn test_cmp_for_agg_ask_eq() {
        let lhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Equal);
    }

    #[test]
    fn test_cmp_for_agg_bid_ask_gt() {
        let lhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Greater);
    }

    #[test]
    fn test_cmp_for_agg_ask_bid_lt() {
        let lhs = AggBookEntry::Ask(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.62,
                amount: 0.25441737,
            },
        });
        let rhs = AggBookEntry::Bid(BookEntry {
            exchange: "Binance".to_owned(),
            book: BookPair {
                price: 28040.15,
                amount: 0.17822639,
            },
        });

        assert_eq!(lhs.cmp(&rhs), Ordering::Less);
    }
}
