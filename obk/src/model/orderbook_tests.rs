#[cfg(test)]
mod orderbook_tests {

    use super::super::*;
    use claim::{assert_err, assert_ok};
    use serde::de::value::{Error as ValueError, StringDeserializer};
    use serde::de::IntoDeserializer;

    #[test]
    fn test_float_deserialization_valid() {
        let num = "0.6754321".to_string();
        let deserializer: StringDeserializer<ValueError> = num.into_deserializer();
        assert_ok!(de_float_from_str(deserializer));
    }

    #[test]
    fn test_float_deserialization_invalid() {
        let num = "0.6754DEAD".to_string();
        let deserializer: StringDeserializer<ValueError> = num.into_deserializer();
        assert_err!(de_float_from_str(deserializer));
    }

    #[test]
    fn test_long_deserialization_valid() {
        let num = "478665432678890".to_string();
        let deserializer: StringDeserializer<ValueError> = num.into_deserializer();
        assert_ok!(de_long_from_str(deserializer));
    }

    #[test]
    fn test_long_deserialization_invalid() {
        let num = "75656565DEADBEEF".to_string();
        let deserializer: StringDeserializer<ValueError> = num.into_deserializer();
        assert_err!(de_float_from_str(deserializer));
    }

    #[test]
    fn test_from_variant_binance() {
        let stream_type = OrderbookStream::BinanceStream(BinanceStream::default());
        assert_eq!(String::from(stream_type), BINANCE_ID.to_string());
    }

    #[test]
    fn test_from_variant_bitstamp() {
        let stream_type = OrderbookStream::BitstampStream(BitstampStream::default());
        assert_eq!(String::from(stream_type), BITSTAMP_ID.to_string());
    }
}
