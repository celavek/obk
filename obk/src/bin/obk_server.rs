use clap::Parser;
use obk::model::orderbook::{
    BinanceStream, BitstampStream, OrderbookStream, BINANCE_WSS_OPTIONS, BINANCE_WSS_URI,
    BITSTAMP_WSS_URI,
};
use tonic::transport::Server;
use tracing::{debug, error, info};

extern crate obk;
use obk::model::{
    aggregate::AggregatorHandle, persistence::AggregatePersistence, streaming::StreamHandle,
};

use obk_rpc::orderbook_aggregator::orderbook_aggregator_server::OrderbookAggregatorServer;
use obk_rpc::server::OrderbookAggregatorImpl;

#[derive(Parser, Debug)]
#[clap(
    version = "0.1",
    author = "Marius Cetateanu",
    about = "Crypto exchanges orderbooks aggregator server"
)]
struct Opts {
    #[clap(short, long, default_value = "ETHBTC")]
    pair: String,
}

const OBK_SERVER_ADDRESS: &str = "[::1]:19042";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    info!("Starting orderbook server ...");
    let opts = Opts::parse();
    let pair: String = opts
        .pair
        .parse()
        .expect("Couldn't parse the crypto symbols pair");
    debug!("Parsed command line arguments {:?}", opts);
    info!("Fetching order book data for pair: {}", pair);

    // Binance connection
    let binance_url = format!(
        "{}{}{}",
        BINANCE_WSS_URI,
        pair.to_lowercase(),
        BINANCE_WSS_OPTIONS
    );
    debug!("Binance URL: {:?}", binance_url);
    let binance_stream = StreamHandle::new(
        &binance_url,
        &pair,
        OrderbookStream::BinanceStream(BinanceStream::default()),
    )
    .await
    .unwrap();
    debug!(
        "Constructed Binance specific stream handle object {:?}",
        binance_stream
    );
    info!("Binance conection established");

    // Bitstamp connection
    let bitstamp_url = BITSTAMP_WSS_URI.to_string();
    debug!("Bitstamp URL: {:?}", bitstamp_url);
    let bitstamp_stream = StreamHandle::new(
        &bitstamp_url,
        &pair,
        OrderbookStream::BitstampStream(BitstampStream::default()),
    )
    .await
    .unwrap();
    debug!(
        "Constructed Bitstamp specific stream handle object {:?}",
        bitstamp_stream
    );
    info!("Bitstamp conection established");

    let agg_handle = AggregatorHandle::new();
    let agg_handle_stream = agg_handle.clone();
    let agg_handle_persist = agg_handle.clone();
    debug!("Instatiated aggregator handles");

    tokio::spawn(async move {
        loop {
            let response = binance_stream.next().await;
            match response {
                Ok(response) => {
                    agg_handle.write(response).await;
                }
                Err(why) => {
                    error!("Failed to get response from the stream object {:?}", why);
                }
            }
        }
    });
    debug!("Spawned tokio task to handle Binance stream");

    tokio::spawn(async move {
        loop {
            let response = bitstamp_stream.next().await;
            match response {
                Ok(response) => {
                    agg_handle_stream.write(response).await;
                }
                Err(why) => {
                    error!("Failed to get response from the stream object {:?}", why);
                }
            }
        }
    });
    debug!("Spawned tokio task to handle Bitstamp stream");

    let orderbook = OrderbookAggregatorImpl {
        persistence: Box::new(AggregatePersistence::new(agg_handle_persist)),
    };
    debug!("Instatiated orderbook aggregator server");

    let addr = OBK_SERVER_ADDRESS.to_string().parse().unwrap();
    info!(
        "Orderbook aggregator server starting. Listening on {}",
        OBK_SERVER_ADDRESS.to_string()
    );
    Server::builder()
        .add_service(OrderbookAggregatorServer::new(orderbook))
        .serve(addr)
        .await?;

    Ok(())
}
