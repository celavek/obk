use tokio::time::{interval, Duration};
use tonic::Request;

use obk_rpc::orderbook_aggregator::orderbook_aggregator_client::OrderbookAggregatorClient;
use obk_rpc::orderbook_aggregator::Empty;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let channel = tonic::transport::Channel::from_static("http://[::1]:19042")
        .connect()
        .await
        .expect("Can't create a channel to server");
    let mut client = OrderbookAggregatorClient::new(channel);

    tokio::spawn(async move {
        let mut interval = interval(Duration::from_millis(100));
        loop {
            interval.tick().await;
            // creating a request
            let request = Request::new(Empty {});
            // sending the request to the server
            let mut response = client.book_summary(request).await.unwrap().into_inner();
            while let Some(res) = response.message().await.unwrap() {
                println!("ORDERBOOK = {:#?}", res);
            }
        }
    })
    .await?;

    Ok(())
}
