pub mod aggregate;
pub mod orderbook;
pub mod persistence;
pub mod streaming;

pub(crate) const BINANCE_ID: &str = "Binance";
pub(crate) const BITSTAMP_ID: &str = "Bitstamp";
