use derivative::Derivative;
use futures::{stream::SplitStream, SinkExt, StreamExt};
use rayon::prelude::*;
use thiserror;
use tokio::{
    net::TcpStream,
    sync::{
        mpsc,
        oneshot::{self, error::RecvError},
    },
};
use tokio_tungstenite::tungstenite::error::Error as TungsteniteError;
use tokio_tungstenite::{tungstenite::Message, MaybeTlsStream, WebSocketStream};
use tracing::{debug, error, instrument, trace, warn};
use url::Url;

use super::aggregate::{AggBookEntry, AggSummary, BookEntry, BookPair};
use super::orderbook::{
    BinanceStream, BitstampStream, OrderbookStream, RawBookEntry, BITSTAMP_WSS_OPTIONS,
};
use super::{BINANCE_ID, BITSTAMP_ID};

/// Possible streaming related errors.
#[derive(thiserror::Error, Debug)]
pub enum StreamError {
    #[error("Failed parsing URL ")]
    UrlParsingError(#[from] url::ParseError),

    #[error("Failed to parse stream")]
    StreamParsingFailed(#[from] serde_json::Error),

    #[error("Connection closed - websocket should be dropped; {0}")]
    ConnectionClosed(String),

    #[error("Failed websocket operation: ")]
    //#[cfg(feature = "async")]
    WebsocketFailure(#[from] TungsteniteError),

    #[error("Stream not available: {0}")]
    UnavailableStream(String),

    #[error("Failed receiving through teh channel")]
    ReceiveFailed(#[from] RecvError),
}

///
/// Messages a stream can receive and handle
///
#[derive(Debug)]
enum StreamMessage {
    GetNext {
        respond_to: oneshot::Sender<Option<(AggSummary, String)>>,
    },
}

/// This is a type alias for a _boxed function_ used for deserialization.
///
/// - The reason we need a Box is because the size of a function is
///   not always guaranteed to be the same from function to function.
///   In order to put a thing in a HashMap, it /must/ have a known
///   size! So we put it behind a layer of indirection by adding
///   Box<...> around it. Boxes are always the same size.
/// - The thing inside the Box has three bounds on it: one is that
///   it is *'static*, which means it lives for the entire length of
///   the program.
/// - Second one is that it is *Send*, meaning it can be shared like
///   sent via a channel
/// - The third is the trait *dyn Fn(Message) -> (AggSummary, String)*,
///	  which itself has two parts: the first argument, which tells you
///   what its argument type is, and the second, which tells you what
///   its output type is. That means it's a function that takes a Message
///   as argument, and produces a *Result<>*: either a tuple of an
///   aggregation summary and a string or a *StreamError* if the
///   deserialization fails.
type Deserializer =
    Box<(dyn Fn(Message) -> Result<(AggSummary, String), StreamError> + Send + 'static)>;

fn make_deserializer<Function>(func: Function) -> Deserializer
where
    Function: Fn(Message) -> Result<(AggSummary, String), StreamError> + Send + 'static,
{
    Box::new(func) as Deserializer
}

///
///  Structure representing the stream of orderbook data from a particular exchange.
///
#[derive(Derivative)]
#[derivative(Debug)]
struct Stream {
    receiver: mpsc::Receiver<StreamMessage>,
    #[derivative(Debug = "ignore")]
    reader: SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>, //@TODO: should I replace this with a BoxStream<_>
    stream_type: OrderbookStream,
    #[derivative(Debug = "ignore")]
    deserializer: Deserializer,
}

impl Stream {
    #[instrument]
    async fn new(
        url_string: &String,
        symbols_pair: &String,
        receiver: mpsc::Receiver<StreamMessage>,
        stream_type: OrderbookStream,
    ) -> Result<Self, StreamError> {
        let (socket, response) = tokio_tungstenite::connect_async(
            Url::parse(url_string).map_err(StreamError::UrlParsingError)?,
        )
        .await
        .map_err(StreamError::WebsocketFailure)?;

        debug!("Connected to {} stream", String::from(stream_type.clone()));
        debug!("HTTP status code: {}", response.status());
        debug!("{:?}", response.headers());

        let (mut writer, mut reader) = socket.split();

        let deserializer = match stream_type {
            OrderbookStream::BinanceStream(_) => {
                make_deserializer(|message: Message| {
                    let message = match message {
                        Message::Text(s) => s,
                        _ => {
                            error!("invalid format received - message not convertible to Message::Text()");
                            "".to_string()
                        }
                    };

                    let binance_stream = serde_json::from_str::<BinanceStream>(&message)
                        .map_err(StreamError::StreamParsingFailed)?;
                    let (bids, asks) = Self::generate_summary(
                        &binance_stream.bids[0..20],
                        &binance_stream.asks[0..20],
                        BINANCE_ID,
                    );

                    Ok((
                        AggSummary {
                            bids,
                            asks,
                            ..Default::default()
                        },
                        BINANCE_ID.to_owned(),
                    ))
                })
            }
            OrderbookStream::BitstampStream(_) => {
                let pair_option = format!(
                    "{}{}{}",
                    "\"order_book_",
                    symbols_pair.to_lowercase(),
                    "\"} }"
                );
                debug!(
                    "Bitstamp websocket options: {} - {}",
                    BITSTAMP_WSS_OPTIONS, pair_option
                );

                writer
                    .send(Message::Text(format!(
                        "{}{}",
                        BITSTAMP_WSS_OPTIONS, pair_option
                    )))
                    .await
                    .map_err(StreamError::WebsocketFailure)?;
                debug!("Sent options to the Bitstamp websocket connection.");

                // ignore the error as for Bitstamp we need to make a dummy read operation first,
                // before trying to get the stream and deserialize!
                let _ = reader.next().await;

                make_deserializer(|message: Message| {
                    let bitstamp_stream =
                        serde_json::from_str::<BitstampStream>(&message.to_string())
                            .map_err(StreamError::StreamParsingFailed)?;

                    let (bids, asks) = Self::generate_summary(
                        &bitstamp_stream.data.bids[0..20],
                        &bitstamp_stream.data.asks[0..20],
                        BITSTAMP_ID,
                    );

                    Ok((
                        AggSummary {
                            bids,
                            asks,
                            ..Default::default()
                        },
                        BITSTAMP_ID.to_owned(),
                    ))
                })
            }
        };

        Ok(Stream {
            receiver,
            reader,
            stream_type,
            deserializer,
        })
    }

    #[instrument]
    async fn handle_message(&mut self, message: StreamMessage) {
        trace!("Handling stream message");
        match message {
            StreamMessage::GetNext { respond_to } => {
                let try_msg = self.reader.next().await;

                if let Some(result) = try_msg {
                    debug!("received data from the network stream.");

                    match result {
                        Ok(message) => match (self.deserializer)(message) {
                            Ok(aggregation) => {
                                let _ = respond_to.send(Some(aggregation));
                            }
                            Err(why) => {
                                error!("Failed datat deserialization {:?}", why);
                                let _ = respond_to.send(None);
                            }
                        },
                        Err(why) => {
                            error!("Failed to get data from the newtork stream {:?}", why);
                            let _ = respond_to.send(None);
                        }
                    }
                } else {
                    warn!("No data received from the network stream - stream might have been closed or killed");

                    let _ = respond_to.send(None);
                }
            }
        }
    }

    fn generate_summary(
        raw_bids: &[RawBookEntry],
        raw_asks: &[RawBookEntry],
        exchange_id: &str,
    ) -> (Vec<AggBookEntry>, Vec<AggBookEntry>) {
        let mut bids: Vec<AggBookEntry> = raw_bids
            .par_iter()
            .map(|el| {
                AggBookEntry::Bid(BookEntry {
                    exchange: exchange_id.to_owned(),
                    book: BookPair {
                        price: el.price,
                        amount: el.amount,
                    },
                })
            })
            .collect();
        bids.par_sort();

        let mut asks: Vec<AggBookEntry> = raw_asks
            .par_iter()
            .map(|el| {
                AggBookEntry::Ask(BookEntry {
                    exchange: exchange_id.to_owned(),
                    book: BookPair {
                        price: el.price,
                        amount: el.amount,
                    },
                })
            })
            .collect();
        asks.par_sort();

        (bids, asks)
    }
}

async fn get_next(mut exchange_stream: Stream) {
    while let Some(msg) = exchange_stream.receiver.recv().await {
        exchange_stream.handle_message(msg).await;
    }
}

#[derive(Clone, Debug)]
pub struct StreamHandle {
    sender: mpsc::Sender<StreamMessage>,
}

impl StreamHandle {
    pub async fn new(
        address: &String,
        symbols_pair: &String,
        stream_type: OrderbookStream,
    ) -> Result<Self, StreamError> {
        let (sender, receiver) = mpsc::channel(16);
        let exchange_stream = Stream::new(address, symbols_pair, receiver, stream_type).await?;

        tokio::spawn(get_next(exchange_stream));

        Ok(Self { sender })
    }

    pub async fn next(&self) -> Result<(AggSummary, String), StreamError> {
        let (tx, rx) = oneshot::channel();
        let msg = StreamMessage::GetNext { respond_to: tx };

        // if the send fails so does the recv.await;
        // check for the error just once
        let _ = self.sender.send(msg).await;

        if let Some(result) = rx.await? {
            Ok(result)
        } else {
            Err(StreamError::UnavailableStream("No data received from the network stream - stream might have been closed or killed".to_owned()))
        }
    }
}

#[cfg(test)]
#[path = "streaming_tests.rs"]
mod streaming_tests;
